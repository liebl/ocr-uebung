transkribiert aus dem Jahrbuch des Schweizer Alpenclubs, siehe:
Clematide, Simon, Lenz Furrer, and Martin Volk. "Crowdsourcing an OCR Gold Standard for a German and French Heritage Corpus."

verfügbar unter:
https://files.ifi.uzh.ch/cl/OCR19thSAC/
