# installation requirements

* 01_ocropy: conda env + ocropy docker (see below)
* 02_tesseract: conda env
* 03_binarize: conda env
* 04_layout: conda env

# running via pip environment

Choose a sensible path for "/path/to/your/ocr-tut-2022-env", e.g. use "$HOME/ocr-tut-2022".

```
python3 -m venv /path/to/your/ocr-tut-2022-env
source /path/to/your/ocr-tut-2022-env/bin/activate

pip install -r /path/to/this/package/requirements.txt
python3 -m bash_kernel.install
jupyter lab
```

# running via conda environment

You need anaconda3 or miniconda3 (see https://docs.conda.io/en/latest/miniconda.html).

```
cd /path/to/this/package
conda env create -f environment.yml

conda activate ocr-tut-2022
python -m bash_kernel.install
jupyter lab
```

# install tesseract (recommended)

Debian: `apt install tesseract-ocr tesseract-ocr-deu tesseract-ocr-fra`
also see https://wiki.ubuntuusers.de/tesseract-ocr/

Windows: `choco install tesseract`
also see https://community.chocolatey.org/packages/tesseract

macOS: `brew install tesseract tesseract-lang`
also see https://formulae.brew.sh/formula/tesseract

# additional tesseract languages (recommended)

Install the German (deu) and French (fra) language pack.

Run `tesseract --list-langs` in a terminal to check which language packs are installed.

See https://ocrmypdf.readthedocs.io/en/latest/languages.html


# ocropy (purely optional)

Install Docker (https://docs.docker.com/get-docker/). Then:

`docker pull kbai/ocropy`

